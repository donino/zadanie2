#include<stdio.h>
#include<stdlib.h>
#include <time.h>



typedef struct TWN{
       float udaj;
       struct TWN *prev;
       struct TWN *next;
}TWN;


typedef struct{
       TWN *cur;
}CTWL;

float ctwl_delete(CTWL* list){
       TWN *s;
       float h;
       
       if(list->cur == NULL){
              return 0;
       }
       if(list->cur->next == list->cur){
              free(list->cur);
              list->cur = NULL;
              return 0;
       }
       
       s = list->cur->prev;
       h = list->cur->udaj;
       list->cur = list->cur->next;
       free(s->next);
       s->next = list->cur;
       list->cur->prev = s;
       
       return h;
       
       
}

TWN *ctwl_insert_right(CTWL* list, float val){
       TWN *vystup, *s;
       
       
       vystup = malloc(sizeof(TWN));
       if(vystup == 0) 
              return NULL;       
       if(list->cur->next != NULL){
              s = list->cur->next;
              s->prev = vystup;
              vystup->next = s;
       }
       list->cur->next = vystup;
       vystup->udaj = val;
       vystup->prev = list->cur;
       
       return vystup;
}

TWN *ctwl_insert_left(CTWL* list, float val){
       TWN *vystup, *s;
       
       
       vystup = malloc(sizeof(TWN));
       if(vystup == 0) 
              return NULL;       
       if(list->cur->prev != NULL){
              s = list->cur->prev;
              s->next = vystup;
              vystup->prev = s;
       }
       list->cur->prev = vystup;
       vystup->udaj = val;
       vystup->next = list->cur;
       
       return vystup;
}


void ctwl_cur_step_right(CTWL *list){
       
       if(list->cur->next != NULL){
              list->cur = list->cur->next;
       }
}

void ctwl_cur_step_left(CTWL *list){
       
       if(list->cur->prev != NULL){
              list->cur = list->cur->prev;
       }
}



CTWL *ctwl_create_empty(void){
       CTWL *vystup;
       
       vystup = malloc(sizeof(CTWL));
       if(vystup == 0){
              return NULL;
       }
       vystup->cur = NULL;
       
       return  vystup;
       
       
}

float randomFloat()
{
      float r = (float)rand() / (float)RAND_MAX;
      return r;
}



CTWL *ctwl_create_random(unsigned int size){
       CTWL *a;
       
       unsigned int i;
      
       a = NULL;
       a = ctwl_create_empty();
       if(a == 0)
              return NULL;
      
       a->cur = malloc(sizeof(TWN));
       if(a->cur == 0){
              return NULL;
       }
          
       a->cur->udaj = randomFloat(); 
       a->cur->next = a->cur;
       a->cur->prev = a->cur;
      
       for (i = 1; i < size; i++){
              
             
              ctwl_insert_right(a, randomFloat());
              ctwl_cur_step_right(a);
              if(a == NULL)
                     return NULL;
   
              
       }
       
      
       
       ctwl_cur_step_right(a); // posunutie na zaciatok
      
       return a;
       
}

void ctwl_print(CTWL *list){
       TWN *p;
       
       printf("\n");
       if(list == NULL){
              printf("Zoznam nenajdeny!!!");
              return;
       }
       if(list->cur == NULL){
              printf("Prazdny zoznam!!!");
              return;
       }
       p = list->cur;
       do {
        printf ("\nprvok: %f ",  p->udaj);
        p = p->next;
    }while (p != list->cur);
       
       
}


void ctwl_inversion(CTWL *list){
       TWN *s, *p;
       
       s = list->cur;
       
       
    
       
       do {
              p = s->next;
              s->next = s->prev;
              s->prev = p;
              s = s->next;
              
       } while (s != list->cur);
       
       
}

void ctwl_destroy(CTWL *list){
       TWN *p;
       
       if(list->cur == NULL){
              free(list);
              list = NULL;
              return;
       }
       
       p = list->cur;
       ctwl_cur_step_right(list);
       while((p != list->cur)){
        ctwl_delete(list);
      
       } 
       ctwl_delete(list);
       free(list);
       list = NULL;
       return;
    
}




int main(){
       /*unsigned int size=10;
       CTWL *zoznam;
       
       srand(time(NULL));
       zoznam = NULL;
       
       
       
             
       zoznam = ctwl_create_random(size);
       if(zoznam == NULL) {
              printf("POZOR NEPODARILO SA ALOKOVAT PAMAT!!!");
              return 0;
       }
       
      
      
       ctwl_print(zoznam);
       ctwl_inversion(zoznam);
       printf("\n prehodenie");
       ctwl_print(zoznam); 
       
       ctwl_cur_step_right(zoznam);
       //ctwl_print(zoznam); 
       
       //printf("\n pridane 22");
       //ctwl_insert_left(zoznam, 22);
       //ctwl_print(zoznam); 
       
       printf("\n po zmazani: %f", ctwl_delete(zoznam));
       
       ctwl_print(zoznam); 
       
       
       ctwl_destroy(zoznam);
       */
       //skusobna cast
       
       CTWL *pokus;

       pokus = ctwl_create_empty();
       pokus = ctwl_create_random(4);
       printf("%x\n", pokus);
       ctwl_destroy(pokus);
       printf("%x\n", pokus);
       ctwl_print(pokus);
       
       return 0;

}

